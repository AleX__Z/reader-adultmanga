package com.AleX_Z.app.manga.mangamenu.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.AleX_Z.app.manga.mangamenu.R;
import com.AleX_Z.app.manga.mangaorm.Exception.SourceMangaException;
import com.AleX_Z.app.manga.mangaorm.History.HistoryExplorer;
import com.AleX_Z.app.manga.mangaorm.Lang.HistoryObject;
import com.AleX_Z.app.manga.mangaorm.SourceManga.PartMangaFeature;
import com.AleX_Z.app.manga.mangaorm.SourceManga.SourceManga;
import com.devspark.progressfragment.ProgressFragment;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardGridArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.internal.CardThumbnail;
import it.gmariotti.cardslib.library.view.CardGridView;

/**
 * Created by Alex on 29.07.2014.
 */
public class FindFragment extends ProgressFragment {
    private static final String SCHEME = "item";
    private static final String AUTHORITY = "search";
    public static final Uri URI = new Uri.Builder()
            .scheme(SCHEME)
            .authority(AUTHORITY)
            .build();
    public final static String TAG = FindFragment.class.getSimpleName();

    public String searchText;
    private View view;
    private CardGridView listView;


    //DisplayImageOptions options;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list_find_manga, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                        //.defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                        //.writeDebugLogs()
                .build();

        ImageLoader.getInstance().init(config);*/

        /*options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                        //.cacheOnDisk(true)
                        //.displayer(new SimpleBitmapDisplayer())
                .showImageOnFail(R.drawable.test)
                .showImageOnLoading(R.drawable.test)
                .build();*/

        if (savedInstanceState != null)
            searchText = savedInstanceState.getString("searchText");
        setContentView(view);
        setContentShown(false);
        setEmptyText("No Find");

    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            //TODO
            HistoryExplorer.addHistory(new HistoryObject("Search by word " + searchText));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        new SearchAsyncTask().execute(searchText);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    class SearchAsyncTask extends AsyncTask<String, Void, List<PartMangaFeature>> {

        @Override
        protected List<PartMangaFeature> doInBackground(String... params) {
            try {
                return SourceManga.search(params[0]);
            } catch (SourceMangaException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<PartMangaFeature> partMangaFeatures) {
            if (isVisible()) {
                ArrayList<Card> cards = new ArrayList<Card>();
                for (PartMangaFeature partMangaFeature : partMangaFeatures)
                    cards.add(new SearchCard(getActivity(), partMangaFeature, null));

                CardGridArrayAdapter mCardArrayAdapter;
                mCardArrayAdapter = new CardGridArrayAdapter(getActivity(), cards);

                listView = (CardGridView) getActivity().findViewById(R.id.cardGridView_find);
                if (listView != null) {
                    listView.setAdapter(mCardArrayAdapter);
                }
                setContentShown(true);
            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //listView.removeAllViewsInLayout();
        //mCardArrayAdapter.clear();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("searchText", searchText);
        super.onSaveInstanceState(outState);
    }

    public class SearchCard extends Card {
        private URL sourceManga;
        private String author;
        private String imageManga;
        private String nameManga;

        DisplayImageOptions options;

        public SearchCard(Context context, PartMangaFeature partMangaFeature, DisplayImageOptions options) {
            super(context, R.layout.menu_card_inner_layout);
            this.sourceManga = partMangaFeature.sourceManga;
            this.author = partMangaFeature.author;
            this.imageManga = partMangaFeature.imageManga;
            this.nameManga = partMangaFeature.nameManga;
            this.options = options;
            initCard();
        }


        private void initCard() {
            CardHeader header = new CardHeader(getContext(), R.layout.menu_card_header_layout);
            header.setTitle(nameManga);
            addCardHeader(header);

            GplayGridThumb thumbnail = new GplayGridThumb(getContext(), imageManga);
            thumbnail.setUrlResource(imageManga);

            addCardThumbnail(thumbnail);


            setOnClickListener(new OnCardClickListener() {
                @Override
                public void onClick(Card card, View view) {
                    final FragmentManager fm = getActivity().getSupportFragmentManager();
                    final FragmentTransaction tr = fm.beginTransaction();
                    SearchCard searchCard = (SearchCard) card;
                    Bundle bundle = new Bundle();
                    InfoMangaFragment fragment = new InfoMangaFragment();
                    fragment.searchMode = true;
                    bundle.putString("linkSearchManga", searchCard.sourceManga.toString());
                    fragment.setArguments(bundle);
                    tr.addToBackStack(null);
                    tr.replace(R.id.content, fragment);
                    tr.commit();
                }
            });

        }

        @Override
        public void setupInnerViewElements(ViewGroup parent, View view) {
            TextView subtitle = (TextView) view.findViewById(R.id.carddemo_cursor_main_inner_subtitle);
            subtitle.setText(author);
        }

        class GplayGridThumb extends CardThumbnail {
            private String url;


            public GplayGridThumb(Context context, String url) {
                super(context);
                this.url = url;
            }


            @Override
            public void setupInnerViewElements(ViewGroup parent, View viewImage) {
                    /*ImageLoader imageLoader = ImageLoader.getInstance();;

                    imageLoader.init(config);
                    ImageView imageView = (ImageView) parent.findViewById(R.id.card_thumbnail_image);
                    final ProgressBar spinner = (ProgressBar) parent.findViewById(R.id.loading);

                    imageView.setImageBitmap(imageLoader.loadImageSync(url));*/


                //imageLoader.displayImage(url,(ImageView) viewImage.findViewById(R.id.card_thumbnail_image));
                //setUrlResource(url);
                //viewImage.getLayoutParams().width = 196;
                //viewImage.getLayoutParams().height = 196;
                    /*ImageView imageView = (ImageView)parent.findViewById(R.id.card_thumbnail_image);

                    imageLoader.displayImage(url,imageView,new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            spinner.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            spinner.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            spinner.setVisibility(View.GONE);
                        }
                    });*/
                /*ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));


                //((ImageView)viewImage).setImageBitmap(imageLoader.loadImageSync(url));
                imageLoader.displayImage(url,(ImageView)viewImage,options );*/

            }

        }


    }
}
