package com.AleX_Z.app.manga.mangamenu;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;


import com.AleX_Z.app.manga.login.LogInActivity;
import com.AleX_Z.app.manga.mangamenu.adapter.ActionMenuAdapter;
import com.AleX_Z.app.manga.mangamenu.fragment.AboutFragment;
import com.AleX_Z.app.manga.mangamenu.fragment.FindFragment;
import com.AleX_Z.app.manga.mangamenu.fragment.HistoryFragment;
import com.AleX_Z.app.manga.mangamenu.fragment.ListUserMangaFragment;
import com.AleX_Z.app.manga.mangaorm.Exception.MangaException;
import com.AleX_Z.app.manga.mangaorm.Lang.DataBase.MangaDao;
import com.AleX_Z.app.manga.mangaorm.Lang.Manga;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;

import shared.ui.actionscontentview.ActionsContentView;


public class MenuActivity extends FragmentActivity {

    private static final String KEY_URI = "state:uri";
    private static final String KEY_FRAGMENT_TAG = "state:fragment_tag";
    public static final Uri UPDATE_URI = Uri.parse("update");

    private String currentContentFragmentTag = null;
    private Uri currentUri = ListUserMangaFragment.URI;
    private ActionsContentView viewActionsContentView;
    private MangaDao mangaDao;
    private List<Manga> mangas;
    private SearchView searchView;
    private MenuItem searchMenuItem;
    private String searchText = "";
    private boolean isRotated = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        viewActionsContentView = (ActionsContentView) findViewById(R.id.actionsContentView);
        viewActionsContentView.setSwipingType(ActionsContentView.SWIPING_EDGE);
        viewActionsContentView.setShadowVisible(false);
        //viewActionsContentView.setSpacingType(ActionsContentView.SPACING_ACTIONS_WIDTH);

        final ListView viewActionsList = (ListView) findViewById(R.id.actions);
        final ActionMenuAdapter actionsAdapter = new ActionMenuAdapter(this);
        viewActionsList.setAdapter(actionsAdapter);
        viewActionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long flags) {
                final Uri uri = actionsAdapter.getItem(position);
                String s = uri.getAuthority();
                if (s.equals("login")) {
                    startActivity(new Intent(getApplicationContext(), LogInActivity.class));
                    return;
                }
                if (!(s.equals("manga") || s.equals("about") || s.equals("history"))) {
                    Toast.makeText(getApplicationContext(), "Don't Work", Toast.LENGTH_SHORT).show();
                    return;
                }


                updateContent(uri);
                viewActionsContentView.showContent();
            }
        });

        if (savedInstanceState != null) {
            currentUri = Uri.parse(savedInstanceState.getString(KEY_URI));
            currentContentFragmentTag = savedInstanceState.getString(KEY_FRAGMENT_TAG);
            isRotated = true;
        }
        if (!isRotated) {
            updateContent(currentUri);
            isRotated = false;
        }
    }

    public void setUriTag(Uri uri, String tag) {
        currentUri = uri;
        currentContentFragmentTag = tag;
    }

    public void onActionsButtonClick(View view) {
        if (viewActionsContentView.isActionsShown())
            viewActionsContentView.showContent();
        else
            viewActionsContentView.showActions();
    }

    public void updateContent(Uri uri) {
        final Fragment fragment;
        final String tag;

        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction tr = fm.beginTransaction();

        clearBackStackFragment(fm);

        if (UPDATE_URI.equals(uri)) {
            fragment = fm.findFragmentByTag(currentContentFragmentTag);
            tr.replace(R.id.content, fragment);
            tr.commit();
            return;
        }

        if (!currentUri.equals(uri)) {
            final Fragment currentFragment = fm.findFragmentByTag(currentUri.toString());
            if (currentFragment != null)
                tr.hide(currentFragment);
        }

        if (FindFragment.URI.equals(uri)) {
            tag = FindFragment.TAG;
            final FindFragment foundFragment = (FindFragment) fm.findFragmentByTag(FindFragment.TAG);
            if (foundFragment != null && (searchText.equals(foundFragment.searchText))) {
                fragment = foundFragment;
            } else {
                fragment = new FindFragment();
                ((FindFragment) fragment).searchText = searchText;
            }

        } else if (AboutFragment.URI.equals(uri)) {
            tag = AboutFragment.TAG;
            final Fragment foundFragment = fm.findFragmentByTag(tag);
            if (foundFragment != null) {
                fragment = foundFragment;
            } else {
                fragment = new AboutFragment();
            }
        } else if (HistoryFragment.URI.equals(uri)) {
            tag = HistoryFragment.TAG;
            final Fragment foundFragment = fm.findFragmentByTag(tag);
            if (foundFragment != null) {
                fragment = foundFragment;
            } else {
                fragment = new HistoryFragment();
            }
        } else if (ListUserMangaFragment.URI.equals(uri)) {
            tag = ListUserMangaFragment.TAG;
            final ListUserMangaFragment foundFragment = (ListUserMangaFragment) fm.findFragmentByTag(tag);
            if (foundFragment != null) {
                fragment = foundFragment;
            } else {
                fragment = new ListUserMangaFragment();
            }
        } else {
            return;
        }


        fragment.setRetainInstance(true);

        if (fragment.isAdded()) {
            tr.show(fragment);
        } else {
            tr.replace(R.id.content, fragment, tag);
        }

        if (!tag.equals(ListUserMangaFragment.TAG))
            tr.addToBackStack(null);

        tr.commit();

        currentContentFragmentTag = tag;
        currentUri = uri;

        if (searchView != null && !tag.equals(FindFragment.TAG))
            searchView.onActionViewCollapsed();

    }

    void clearBackStackFragment(FragmentManager fm) {
        for (int i = 0, size = fm.getBackStackEntryCount(); i < size; i++)
            fm.popBackStack();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(KEY_URI, currentUri.toString());
        outState.putString(KEY_FRAGMENT_TAG, currentContentFragmentTag);
        super.onSaveInstanceState(outState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);
        final SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.search);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                searchText = query;
                updateContent(FindFragment.URI);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        if (isRotated && currentContentFragmentTag.equals(FindFragment.TAG)) {
            searchView.onActionViewExpanded();
            searchView.clearFocus();
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.refresh).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);

        /*if (i == R.id.refresh) {

        } else {

        }*/
        /*if (i == R.id.add_manga) {
            final Context context = this;
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setTitle("Add Manga");
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.VERTICAL);
            final Spinner spinner = new Spinner(context);

            List<String > data = SourceManga.hosts;
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
            spinner.setAdapter(adapter);
            spinner.setSelection(0);
            layout.addView(spinner);
            final EditText linkBox = new EditText(context);
            linkBox.setHint("Continue link");
            layout.addView(linkBox);
            dialog.setView(layout);
            dialog.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new AddMangaAsyncTask().execute("http://" + spinner.getSelectedItem().toString() + "/" + linkBox.getText().toString());
                }
            });
            dialog.show();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }*/

    }

    class AddMangaAsyncTask extends AsyncTask<String, Void, Manga> {

        @Override
        protected Manga doInBackground(String... params) {
            try {
                return new Manga(new URL(params[0])).addDataBase().updateChapters();
            } catch (IOException | SQLException | MangaException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Manga manga) {
            super.onPostExecute(manga);
            updateContent(currentUri);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(FindFragment.TAG);
        if (fragment != null && fragment.isVisible())
            searchView.onActionViewCollapsed();

        super.onBackPressed();
    }

}
