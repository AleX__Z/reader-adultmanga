package com.AleX_Z.app.manga.dzenreader;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.view.Window;
import android.widget.Toast;

import com.AleX_Z.app.manga.dzen.R;
import com.AleX_Z.app.manga.mangaorm.Exception.NoFoundSourceMangaException;
import com.AleX_Z.app.manga.mangaorm.Exception.SourceMangaException;
import com.AleX_Z.app.manga.mangaorm.History.HistoryExplorer;
import com.AleX_Z.app.manga.mangaorm.Lang.Chapter;
import com.AleX_Z.app.manga.mangaorm.Lang.HistoryObject;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.io.IOException;
import java.sql.SQLException;


/**
 * Created by Alex on 15.07.2014.
 */
public class DzenActivity extends Activity {

    public static final int
            START_GET_LINKS = 0,
            END_GET_LINKS = 1,
            GET_BITMAP = 2;


    public static int COUNT_THREAD = 3;
    //DzenThread[] threads;

    private int curPage = 0;
    Handler handler;
    private Chapter chapter;
    public String[] linksPage;
    private PullToRefreshDzenViewPager viewPager;
    private ImageLoader imageLoader;
    //private Bitmap[] bitmaps;

    Runnable runnableGetLinks = new Runnable() {
        @Override
        public void run() {
            try {
                linksPage = chapter.getLinksPages();
            } catch (IOException | NoFoundSourceMangaException | SQLException e) {
                e.printStackTrace();
            } catch (SourceMangaException e) {
                e.printStackTrace();
            }
            handler.sendEmptyMessage(END_GET_LINKS);
        }
    };
    Thread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        //requestWindowFeature(Window.FEATURE_PROGRESS);
        getActionBar().hide();

        getActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.dzen);
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.akumetsu)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                //.imageScaleType(ImageScaleType.NONE)
                .build();


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(defaultOptions)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        if (chapter == null)
            chapter = (Chapter) getIntent().getSerializableExtra("chapter");

        viewPager = (PullToRefreshDzenViewPager) findViewById(R.id.dzenViewPager1);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case START_GET_LINKS:
                        try {
                            HistoryExplorer.addHistory(
                                    new HistoryObject(
                                            new StringBuilder()
                                                    .append("Read ")
                                                    .append(chapter.getManga().getNames())
                                                    .append(" №")
                                                    .append(chapter.getNumberChapter())
                                                    .toString()
                                    )
                            );
                        } catch (SQLException e) {
                            //TODO
                            e.printStackTrace();
                        }
                        thread = new Thread(runnableGetLinks);
                        thread.start();
                        break;
                    case END_GET_LINKS:
                        viewPager.getRefreshableView().setOffscreenPageLimit(linksPage.length - 1);
                        viewPager.getRefreshableView().setAdapter(new DzenPagerAdapter(getApplicationContext(), linksPage, imageLoader));
                        viewPager.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<DzenViewPager>() {
                            @Override
                            public void onRefresh(PullToRefreshBase<DzenViewPager> dzenViewPagerPullToRefreshBase) {
                                switch (dzenViewPagerPullToRefreshBase.getCurrentMode()) {
                                    case PULL_FROM_START:

                                        if (chapter.getIdPrevChapter() == 0)
                                            Toast.makeText(DzenActivity.this, "Start", Toast.LENGTH_SHORT).show();
                                        else {
                                            chapter = chapter.getPrevChapter();
                                            handler.sendEmptyMessage(START_GET_LINKS);
                                        }
                                        break;
                                    case PULL_FROM_END:

                                        if (chapter.getIdNextChapter() == 0)
                                            Toast.makeText(DzenActivity.this, "End", Toast.LENGTH_SHORT).show();
                                        else {
                                            chapter = chapter.getNextChapter();

                                            handler.sendEmptyMessage(START_GET_LINKS);
                                        }
                                        break;
                                }
                                dzenViewPagerPullToRefreshBase.onRefreshComplete();
                            }
                        });
                        viewPager.getRefreshableView().setCurrentItem(curPage);

                        break;
                    case GET_BITMAP:
                        break;
                }
            }
        };
        handler.sendEmptyMessage(START_GET_LINKS);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable("chapter", chapter);
        outState.putInt("cur", viewPager.getRefreshableView().getCurrentItem());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        //super.onRestoreInstanceState(savedInstanceState);
        curPage = savedInstanceState.getInt("cur", 0);
        chapter = (Chapter) savedInstanceState.getSerializable("chapter");

    }

    @Override
    protected void onDestroy() {
        //System.gc();
        imageLoader.clearMemoryCache();
        DzenViewPager refreshableView = viewPager.getRefreshableView();
        refreshableView.removeAllViewsInLayout();
        super.onDestroy();
    }

    /*private class DzenThread extends Thread {
        private int count;

        public DzenThread(int number) {
            this.count = number;
            start();
        }

        @Override
        public synchronized void start() {
            while (count < linksPage.length) {
                bitmaps[count] = imageLoader.loadImageSync(linksPage[count]);
                Message message = new Message();
                message.what = GET_BITMAP;
                message.arg1 = count;
                handler.sendMessage(message);
                count += COUNT_THREAD;
            }

        }
    }*/
}