package com.AleX_Z.app.manga.mangaorm.Lang;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by Alex on 23.09.2014.
 */
@DatabaseTable(tableName = "history")
public class HistoryObject implements Parcelable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.STRING)
    String name;

    @DatabaseField(dataType = DataType.STRING)
    String description;

    @DatabaseField(dataType = DataType.DATE)
    Date date;

    @DatabaseField(dataType = DataType.STRING)
    String url_action;

    HistoryObject() {

    }

    public HistoryObject (String name) {
        this(name,null,new Date(),null);
    }

    public HistoryObject(String name, String description, Date date, String url_action) {
        this.name = name;
        this.description = description;
        this.date = date;
        this.url_action = url_action;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUrl_action() {
        return url_action;
    }

    public void setUrl_action(String url_action) {
        this.url_action = url_action;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeLong(date != null ? date.getTime() : -1);
        dest.writeString(this.url_action);
    }

    private HistoryObject(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.description = in.readString();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        this.url_action = in.readString();
    }

    public static final Parcelable.Creator<HistoryObject> CREATOR = new Parcelable.Creator<HistoryObject>() {
        public HistoryObject createFromParcel(Parcel source) {
            return new HistoryObject(source);
        }

        public HistoryObject[] newArray(int size) {
            return new HistoryObject[size];
        }
    };
}
