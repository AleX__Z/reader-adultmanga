package com.AleX_Z.app.manga.login;

import android.accounts.*;
import android.os.Parcel;

/**
 * Created by Alex on 29.09.2014.
 */
public class GroupleAccount extends Account {
    public static final String TYPE = "com.AleX_Z.app.manga";

    public static final String TOKEN_FULL_ACCESS = "com.AleX_Z.app.manga.TOKEN_FULL_ACCESS";

    public static final String KEY_PASSWORD = "com.AleX_Z.app.manga.KEY_PASSWORD";


    public GroupleAccount(Parcel in) {
        super(in);
    }

    public GroupleAccount(String name ) {
        super(name, TYPE);
    }


}
