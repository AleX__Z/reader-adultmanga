package com.AleX_Z.app.manga.dzenreader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class DzenImageAdapter extends BaseAdapter {
    private Context context;
    private int height;
    private Bitmap[] bitmaps;

    DzenImageAdapter(Context context, Bitmap[] bitmaps) {
        this.bitmaps = bitmaps;
        this.context = context;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
    }

    public int getCount() {
        return bitmaps.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(getBitmapFromChooseView(bitmaps[position], height / 3));
        return imageView;
    }

    private Bitmap getBitmapFromChooseView(Bitmap oldBitmap, int newHeight) {
        return Bitmap.createScaledBitmap(
                oldBitmap,
                oldBitmap.getWidth() * newHeight / oldBitmap.getHeight(),
                newHeight,
                false
        );
    }
}
