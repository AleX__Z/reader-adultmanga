package com.AleX_Z.app.manga.login;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Alex on 28.09.2014.
 */
public class AccountService extends Service {
    private GroupleAuthenticator authenticator;

    @Override
    public void onCreate() {
        super.onCreate();
        authenticator = new GroupleAuthenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return authenticator.getIBinder();
    }

}
