package com.AleX_Z.app.manga.mangaorm.SourceManga;

import com.AleX_Z.app.manga.mangaorm.Exception.SourceMangaException;
import com.AleX_Z.app.manga.mangaorm.Lang.Chapter;
import com.AleX_Z.app.manga.mangaorm.Lang.Manga;

import java.net.URL;
import java.util.List;

/**
 * Created by Alex on 20.08.2014.
 */
public interface ISourceManga {
    public String[] getHosts();

    public Manga getManga(URL sourceManga, Manga manga) throws SourceMangaException;

    public List<Chapter> getChapters(URL url) throws SourceMangaException;

    public String getLinksPagesChapter(URL sourceChapter) throws SourceMangaException;

    public List<PartMangaFeature> search(String searchText, String host) throws SourceMangaException;
}
