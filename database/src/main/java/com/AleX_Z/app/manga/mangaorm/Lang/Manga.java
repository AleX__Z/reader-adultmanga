package com.AleX_Z.app.manga.mangaorm.Lang;

import com.AleX_Z.app.manga.mangaorm.Exception.MangaException;
import com.AleX_Z.app.manga.mangaorm.Exception.SourceMangaException;
import com.AleX_Z.app.manga.mangaorm.Lang.DataBase.HelperFactory;
import com.AleX_Z.app.manga.mangaorm.SourceManga.SourceManga;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


@DatabaseTable(tableName = "mangas")
public class Manga implements Serializable {
    public static final int INTERNET = 0;
    public static final int DATABASE = 1;

    @DatabaseField(generatedId = true)
    private int id;


    @DatabaseField(dataType = DataType.STRING)
    String sourceManga;

    @DatabaseField(dataType = DataType.STRING)
    String linksMainImageManga;

    @DatabaseField(dataType = DataType.STRING)
    String names;

    @DatabaseField(dataType = DataType.STRING)
    String authors;

    @DatabaseField(dataType = DataType.STRING)
    String translators;

    @DatabaseField(dataType = DataType.INTEGER)
    int year;

    @DatabaseField(dataType = DataType.INTEGER)
    int countChapter;

    @DatabaseField(dataType = DataType.INTEGER)
    int volumes;

    @DatabaseField(dataType = DataType.BOOLEAN)
    boolean translation;

    @DatabaseField(dataType = DataType.BOOLEAN)
    boolean completed;

    //TODO make the lazyCollection in future
    @ForeignCollectionField(eager = true)
    ForeignCollection<Chapter> chapters;

    @DatabaseField(dataType = DataType.INTEGER)
    int idLastChapter;

    @DatabaseField(dataType = DataType.INTEGER)
    int idFirstChapter;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    LanguageTranslate languageTranslate;

    @DatabaseField(dataType = DataType.STRING)
    String genres;

    @DatabaseField(dataType = DataType.STRING)
    String description;

    @DatabaseField(canBeNull = true, dataType = DataType.STRING)
    String moreInfo;

    @DatabaseField(dataType = DataType.DATE)
    Date dateRead;

    @DatabaseField(dataType = DataType.DATE)
    Date dateUpdate;

    @DatabaseField(dataType = DataType.DATE)
    Date dateCreate;

    public Manga() {

    }


    public Manga(URL url) throws IOException, SourceMangaException, SQLException {
        SourceManga.getManga(url, this);
    }


    public Manga addDataBase() {
        try {
            HelperFactory.getInstance().getHelper().getMangaDAO().create(this);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public synchronized List<Chapter> getChapters(int sourceConst, boolean reverseOrder) throws MalformedURLException, MangaException {
        List<Chapter> list;
        switch (sourceConst) {
            case INTERNET:
                list = SourceManga.getChapters(new URL(sourceManga)); //reverse (first element - last chapter)
                if (!reverseOrder)
                    Collections.reverse(list);
                return list;
            case DATABASE:
                if (id == 0)
                    throw new MangaException();
                int size = chapters.size();
                list = new ArrayList<>(size);
                int location = (reverseOrder) ? 0 : size;
                for (Chapter chapter : chapters) list.add(location, chapter);
                return list;
        }
        return null;

    }

    public Manga updateChapters() throws SQLException, MalformedURLException, MangaException {
        //TODO
        if (id == 0)
            throw new MangaException();
        if (chapters == null) {
            chapters = HelperFactory.getInstance().getHelper().getMangaDAO().getEmptyForeignCollection("chapters");
        }
        chapters.clear();

        List<Chapter> list = SourceManga.getChapters(new URL(sourceManga)); //reverse (first element - last chapter)

        if (list.isEmpty())
            return null;

        Collections.reverse(list);
        countChapter = 0;
        return addChapters(list);
    }

    public synchronized Manga updateAll() throws MalformedURLException, MangaException, SQLException {
        if (id == 0)
            throw new MangaException();
        SourceManga.getManga(new URL(sourceManga), this);
        this.updateChapters();
        return this;
    }

    public Manga addChapter(Chapter chapter) throws MangaException, SQLException {
        if (id == 0)
            throw new MangaException();
        Chapter lastChapter = getLastChapter();
        if (lastChapter == null) {
            chapter.setManga(this);
            chapters.add(chapter);
            idFirstChapter = chapter.getId();
            idLastChapter = chapter.getId();
        } else {
            chapter.setIdPrevChapter(lastChapter.getId());
            chapter.setManga(this);
            chapters.add(chapter);
            lastChapter.setIdNextChapter(chapter.getId());
            try {
                chapters.update(lastChapter);
            } catch (SQLException e) {
                throw new MangaException();
            }
            idLastChapter = chapter.getId();
        }
        countChapter++;
        setVolumes(chapter.getVolume());
        HelperFactory.getInstance().getHelper().getMangaDAO().update(this);

        return this;
    }

    public Manga addChapters(List<Chapter> chapterList) throws MangaException, SQLException {
        if (id == 0)
            throw new MangaException();
        for (Chapter aChapterList : chapterList) this.addChapter(aChapterList);
        return this;
    }


    public String[] getLinksMainImageManga() {
        return linksMainImageManga.split(",");
    }

    public void setLinksMainImageManga(String linksMainImageManga) {
        this.linksMainImageManga = linksMainImageManga;
    }


    public void removeChapter(Chapter chapter) throws SQLException {
        chapters.remove(chapter);
        HelperFactory.getInstance().getHelper().getChapterDAO().delete(chapter);
    }

    public void removeChapters() {
        chapters.clear();
    }

    public Chapter getChapterByIndex(int index) throws SQLException {
        int size = countChapter;
        if (size <= index) throw new IllegalArgumentException();
        Iterator iterator = chapters.iterator();
        for (int i = 0; i < index; i++) iterator.next();
        Chapter chapter = (Chapter) iterator.next();
        HelperFactory.getInstance().getHelper().getChapterDAO().refresh(chapter);
        return chapter;
    }

    public Chapter getLastChapter() throws MangaException {
        Chapter chapter = null;
        try {
            if (idLastChapter == 0)
                return null;
            chapter = HelperFactory.getInstance().getHelper().getChapterDAO().queryForId(idLastChapter);
            return chapter;
        } catch (SQLException e) {
            throw new MangaException();
        }
    }

    public boolean withinDB() {
        return id == 0;
    }

    public int getIdLastChapter() {
        return idLastChapter;
    }

    public void setIdLastChapter(int idLastChapter) {
        this.idLastChapter = idLastChapter;
    }

    public int getIdFirstChapter() {
        return idFirstChapter;
    }

    public void setIdFirstChapter(int idFirstChapter) {
        this.idFirstChapter = idFirstChapter;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) throws SQLException {
        this.dateUpdate = dateUpdate;
    }

    public Date getDateRead() {
        return dateRead;
    }

    public void setDateRead(Date dateRead) {
        this.dateRead = dateRead;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSourceManga() {
        return sourceManga;
    }

    public void setSourceManga(String sourceManga) {
        this.sourceManga = sourceManga;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getTranslators() {
        return translators;
    }

    public void setTranslators(String translators) {
        this.translators = translators;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getCountChapter() {
        return countChapter;
    }

    public void setCountChapter(int countChapter) {
        this.countChapter = countChapter;
    }

    public int getVolumes() {
        return volumes;
    }

    public void setVolumes(int volumes) {
        this.volumes = volumes;
    }

    public boolean isTranslation() {
        return translation;
    }

    public void setTranslation(boolean translation) {
        this.translation = translation;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public LanguageTranslate getLanguageTranslate() {
        return languageTranslate;
    }

    public void setLanguageTranslate(LanguageTranslate languageTranslate) {
        this.languageTranslate = languageTranslate;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(String moreInfo) {
        this.moreInfo = moreInfo;
    }

}







