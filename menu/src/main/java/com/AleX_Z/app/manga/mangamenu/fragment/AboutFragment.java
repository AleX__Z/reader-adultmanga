package com.AleX_Z.app.manga.mangamenu.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.AleX_Z.app.manga.mangamenu.R;

/**
 * Created by Alex on 29.07.2014.
 */
public class AboutFragment extends Fragment implements View.OnClickListener {

    private static final String SCHEME = "item";
    private static final String AUTHORITY = "about";
    public static final Uri URI = new Uri.Builder()
            .scheme(SCHEME)
            .authority(AUTHORITY)
            .build();
    public final static String TAG = AboutFragment.class.getSimpleName();


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        view.findViewById(R.id.imageButton).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        final FragmentManager fm = getActivity().getSupportFragmentManager();
        final FragmentTransaction tr = fm.beginTransaction();
        ListLibrariesFragment fragment = new ListLibrariesFragment();
        tr.addToBackStack(null);
        tr.replace(R.id.content, fragment);
        tr.commit();
    }
}
