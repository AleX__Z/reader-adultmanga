package com.AleX_Z.app.manga.mangaorm.Lang.DataBase;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import android.content.Context;

public class HelperFactory {

    private static volatile HelperFactory instance;
    private DatabaseHelper databaseHelper;

    private HelperFactory() {

    }


    public static HelperFactory getInstance() {
        if (instance == null)
            synchronized (HelperFactory.class) {
                if (instance == null) instance = new HelperFactory();
            }
        return instance;
    }

    public void init(Context context) {
        if (databaseHelper == null)
            databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
    }

    public void release() {
        if (databaseHelper != null)
            OpenHelperManager.releaseHelper();
        databaseHelper = null;
    }

    public DatabaseHelper getHelper() {
        return databaseHelper;
    }
}
