package com.AleX_Z.app.manga.mangaorm.Lang.DataBase;


import com.AleX_Z.app.manga.mangaorm.Lang.Chapter;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by Alex on 26.06.2014.
 */
public class ChapterDao extends BaseDaoImpl<Chapter, Integer> {
    protected ChapterDao(ConnectionSource connectionSource,
                         Class<Chapter> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
