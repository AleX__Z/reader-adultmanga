package com.AleX_Z.app.manga.mangaorm.SourceManga;

import java.net.URL;

/**
 * Created by Alex on 20.08.2014.
 */
public class PartMangaFeature {
    public URL sourceManga;
    public String author;
    public String imageManga;
    public String nameManga;

    public PartMangaFeature(URL sourceManga, String author, String imageManga, String nameManga) {
        this.sourceManga = sourceManga;
        this.author = author;
        this.imageManga = imageManga;
        this.nameManga = nameManga;
    }
}
