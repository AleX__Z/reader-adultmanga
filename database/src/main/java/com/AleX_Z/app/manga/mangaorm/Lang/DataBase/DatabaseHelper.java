package com.AleX_Z.app.manga.mangaorm.Lang.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.AleX_Z.app.manga.mangaorm.Lang.Chapter;
import com.AleX_Z.app.manga.mangaorm.Lang.HistoryObject;
import com.AleX_Z.app.manga.mangaorm.Lang.Manga;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();
    private static final String DATABASE_NAME = "user_manga.db";
    private static final int DATABASE_VERSION = 27;

    private MangaDao mangaDao = null;
    private ChapterDao chapterDao = null;
    private HistoryDao historyDao = null;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Manga.class);
            TableUtils.createTable(connectionSource, Chapter.class);
            TableUtils.createTable(connectionSource, HistoryObject.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer) {
        try {
            TableUtils.dropTable(connectionSource, Manga.class, true);
            TableUtils.dropTable(connectionSource, Chapter.class, true);
            TableUtils.dropTable(connectionSource, HistoryObject.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ChapterDao getChapterDAO() throws SQLException {
        if (chapterDao == null) {
            chapterDao = new ChapterDao(getConnectionSource(), Chapter.class);
        }
        return chapterDao;
    }

    public MangaDao getMangaDAO() throws SQLException {
        if (mangaDao == null) {
            mangaDao = new MangaDao(getConnectionSource(), Manga.class);
        }
        return mangaDao;
    }

    public HistoryDao getHistoryDAO() throws SQLException {
        if (historyDao == null) {
            historyDao = new HistoryDao(getConnectionSource(), HistoryObject.class);
        }
        return historyDao;
    }

    @Override
    public void close() {
        super.close();
        chapterDao = null;
        mangaDao = null;
    }
}