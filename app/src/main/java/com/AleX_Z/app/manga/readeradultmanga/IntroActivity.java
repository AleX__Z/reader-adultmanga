package com.AleX_Z.app.manga.readeradultmanga;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.AleX_Z.app.manga.login.AccountExplorer;
import com.AleX_Z.app.manga.mangamenu.MenuActivity;

/**
 * Created by Alex on 26.07.2014.
 */
public class IntroActivity extends Activity {

    public static int TIME_INTRO;
    private IntroThread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        TIME_INTRO = (BuildConfig.DEBUG) ? 100 : 1600;
        thread = new IntroThread();
        thread.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        thread.toggle();
    }

    private class IntroThread extends Thread {
        public volatile boolean running = true;

        @Override

        public void run() {
            try {
                sleep(TIME_INTRO);
            } catch (InterruptedException ignored) {
            } finally {
                synchronized ((Boolean) running) {
                    if (running) {
                        //AccountExplorer.getInstance().
                        startActivity(new Intent(getApplicationContext(), MenuActivity.class));
                        finish();
                    }
                }
            }
        }

        public void toggle() {
            synchronized ((Boolean) running) {
                running = !running;
            }
        }
    }
}
