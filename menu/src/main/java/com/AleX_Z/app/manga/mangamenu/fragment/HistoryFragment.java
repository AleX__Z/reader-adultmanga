package com.AleX_Z.app.manga.mangamenu.fragment;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;

import com.AleX_Z.app.manga.mangamenu.R;
import com.AleX_Z.app.manga.mangaorm.History.HistoryExplorer;
import com.AleX_Z.app.manga.mangaorm.Lang.HistoryObject;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Alex on 23.09.2014.
 */
public class HistoryFragment extends ListFragment {
    private static final String SCHEME = "item";
    private static final String AUTHORITY = "history";
    public static final Uri URI = new Uri.Builder()
            .scheme(SCHEME)
            .authority(AUTHORITY)
            .build();

    public static String TAG = HistoryFragment.class.getSimpleName();
    private List<HistoryObject> list;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history,container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //TODO Async,com.AleX_Z.app.manga.login.Exception
        try {
            list = HistoryExplorer.getListHistory();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Collections.reverse(list);

        ArrayList<HashMap<String, String>> myArrList = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map;
        SimpleDateFormat f = new SimpleDateFormat("HH:mm dd.MM.yy");

        for (HistoryObject h : list) {
            map = new HashMap<String, String>();
            map.put("Name", h.getName());
            map.put("Date", f.format(h.getDate()));
            myArrList.add(map);
        }

        setListAdapter( new SimpleAdapter(getActivity(), myArrList, R.layout.item_list_2,
                new String[] {"Name", "Date"},
                new int[] {android.R.id.text1, android.R.id.text2}));
    }
}
