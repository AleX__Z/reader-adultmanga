package com.AleX_Z.app.manga.dzenreader;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ProgressBar;
import android.widget.ViewSwitcher;

import com.AleX_Z.app.manga.dzen.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;

import uk.co.senab.photoview.PhotoView;

/**
 * Created by Alex on 05.07.2014.
 */
public class DzenPagerAdapter extends PagerAdapter {
    String[] links;
    Context context;
    private ImageLoader imageLoader;

    public DzenPagerAdapter(Context context, String[] links, ImageLoader imageLoader) {
        this.links = links;
        this.context = context;
        this.imageLoader = imageLoader;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final ViewSwitcher viewSwitcher = new ViewSwitcher(context);
        viewSwitcher.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        ProgressBar progressBar = new ProgressBar(context);
        viewSwitcher.addView(progressBar);
        final PhotoView photoView = new PhotoView(context);
        photoView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        viewSwitcher.addView(photoView);

        /*Picasso.with(context).load(links[position]).into(photoView);

        viewSwitcher.showNext();*/
        imageLoader.displayImage(links[position], photoView, new SimpleImageLoadingListener() {


            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                viewSwitcher.showPrevious();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                viewSwitcher.showNext();
            }
        });

        /*PhotoView photoView = new PhotoView(context);
        photoView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        photoView.setImageBitmap(imageLoader.loadImageSync(links[position]));*/
        // photoView.setImageResource(R.drawable.akumetsu);
        container.addView(viewSwitcher);

        return viewSwitcher;
    }

    @Override
    public int getCount() {
        return links.length;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
        view = null;
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }
}
