package com.AleX_Z.app.manga.login;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.graphics.Bitmap;

import com.AleX_Z.app.manga.login.Exception.AccountException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 25.09.2014.
 */
public class AccountExplorer {
	public static final String LINK_LIBRARY = "http://grouple.ru/private/bookmarks";
    public static final String LINK_LOGIN = "http://grouple.ru/j_spring_security_check";
    public static final int READMANGA = 1;
    public static final int ADULTMANGA = 2;
    //public static final int SELFMANGA = 4;



    private static AccountExplorer accountExplorer;
	private HttpClient httpclient;
	private HttpContext httpContext;
	private boolean isLogIn;


	private AccountExplorer() {
		isLogIn = false;
	}

	public static AccountExplorer getInstance() {
		if (accountExplorer == null) accountExplorer = new AccountExplorer();
		return accountExplorer;
	}

    public static void release() {
        accountExplorer = null;
    }

    public Account getAccount(Context context) {
        return getAccounts(context)[0];
    }

    public Account[] getAccounts(Context context) {
        return AccountManager.get(context).getAccountsByType(GroupleAccount.TYPE);
    }

    public AccountExplorer logIn(Context context, GroupleAccount groupleAccount) {
        return logIn(groupleAccount.name,AccountManager.get(context).getPassword(groupleAccount));
    }

	public AccountExplorer logIn(String nameOrEmail, String password)  {
        httpclient = new DefaultHttpClient();
        httpContext = new BasicHttpContext();
        try {
            List<NameValuePair> passForm = new ArrayList<NameValuePair>();
            passForm.add(new BasicNameValuePair("targetUri", ""));
            passForm.add(new BasicNameValuePair("cross", ""));
            passForm.add(new BasicNameValuePair("j_username", nameOrEmail));
            passForm.add(new BasicNameValuePair("j_password", password));
            passForm.add(new BasicNameValuePair("_remember_me", ""));
            passForm.add(new BasicNameValuePair("remember_me", "on"));
            HttpPost postPass = new HttpPost(LINK_LOGIN);
            postPass.setEntity(new UrlEncodedFormEntity(passForm));
            HttpResponse execute = httpclient
                    .execute(postPass, httpContext);
            isLogIn = true;
        } catch (IOException e) {
            isLogIn = false;
        }
        //EntityUtils.consumeQuietly(execute.getEntity());
		return this;
	}

	public AccountExplorer logOut() {
        httpclient = null;
        httpContext = null;
		isLogIn = false;
		return this;
	}

	public boolean isLogIn() throws UnsupportedEncodingException {
		return isLogIn;
	}

    public String[] getTags() {
        String [] tags = new String[0];

        return tags;
    }



	public List<String> getListManga() throws IOException {
		ArrayList<String> name_manga = new ArrayList<String>();
		HttpResponse response = httpclient.execute(new HttpGet(LINK_LIBRARY), httpContext);
		String GET = null;
		try {
			GET = readToEnd(
					new BufferedReader(
							new InputStreamReader(
									response
											.getEntity()
											.getContent()
							)
					)
			);
			//EntityUtils.consumeQuietly(response.getEntity());
		} catch (IOException e) {
			e.printStackTrace();
		}
		Document doc = Jsoup.parse(GET);
		Elements elements = doc.select("tr.bookmark-row>td:nth-child(2)>a:nth-child(2)");
		for (Element element : elements) {
			String name = element.ownText();
			name_manga.add(name);
			//link_manga.add(element.attr("href"));
		}
		return name_manga;
	}

    /*public Bitmap getIconAccount() throws AccountException {
        if (!isLogIn) throw new AccountException();



    }*/



	private String readToEnd(BufferedReader r) throws IOException {
		StringBuilder sb = new StringBuilder();
		String s;
		final String s1 = "\r\n";
		while ((s = r.readLine()) != null) {
			sb.append(s);
			sb.append(s1);
		}
		return sb.toString();
	}

}
