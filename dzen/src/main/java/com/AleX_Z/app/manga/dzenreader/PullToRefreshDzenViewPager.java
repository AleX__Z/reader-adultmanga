package com.AleX_Z.app.manga.dzenreader;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.AttributeSet;

import com.AleX_Z.app.manga.dzen.R;
import com.AleX_Z.app.manga.dzenreader.DzenViewPager;
import com.handmark.pulltorefresh.library.PullToRefreshBase;

/**
 * Created by Alex on 13.08.2014.
 */
public class PullToRefreshDzenViewPager extends PullToRefreshBase<DzenViewPager> {
    public PullToRefreshDzenViewPager(Context context) {
        super(context);
    }

    public PullToRefreshDzenViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public final Orientation getPullToRefreshScrollDirection() {
        return Orientation.HORIZONTAL;
    }

    @Override
    protected DzenViewPager createRefreshableView(Context context, AttributeSet attrs) {
        DzenViewPager viewPager = new DzenViewPager(context, attrs);
        //viewPager.setId(R.id.dzenViewPager1);
        return viewPager;
    }

    @Override
    protected boolean isReadyForPullStart() {
        DzenViewPager refreshableView = getRefreshableView();

        PagerAdapter adapter = refreshableView.getAdapter();
        if (null != adapter) {
            return refreshableView.getCurrentItem() == 0;
        }

        return false;
    }

    @Override
    protected boolean isReadyForPullEnd() {
        DzenViewPager refreshableView = getRefreshableView();
        PagerAdapter adapter = refreshableView.getAdapter();
        if (null != adapter) return refreshableView.getCurrentItem() == adapter.getCount() - 1;
        return false;
    }
}
