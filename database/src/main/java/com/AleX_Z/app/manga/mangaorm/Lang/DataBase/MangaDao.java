package com.AleX_Z.app.manga.mangaorm.Lang.DataBase;

import com.AleX_Z.app.manga.mangaorm.Lang.Manga;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

public class MangaDao extends BaseDaoImpl<Manga, Integer> {
    protected MangaDao(ConnectionSource connectionSource,
                       Class<Manga> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
