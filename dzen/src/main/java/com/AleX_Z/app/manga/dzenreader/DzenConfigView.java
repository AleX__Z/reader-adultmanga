package com.AleX_Z.app.manga.dzenreader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringSystem;
import com.facebook.rebound.SpringUtil;
import com.facebook.rebound.ui.Util;



/**
 * Created by Alex on 16.07.2014.
 */
public class DzenConfigView extends FrameLayout {
    private Spring spring;
    private Bitmap[] bitmaps;
    private DzenViewPager dzenViewPager = null;
    private int height;
    private Context context;
    private DzenSpringListener newListener;
    private float y;
    private float x;
    private boolean checkEventMove;
    private boolean permisionOpenConfigView;


    public DzenConfigView(Context context) {
        this(context, null, 0);
    }

    public DzenConfigView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DzenConfigView(Context context, AttributeSet attrs, int defStyle) {
        this(context, (Bitmap[]) null);
    }

    public DzenConfigView(Context context, Bitmap[] bitmaps) {
        super(context, null, 0);
        this.bitmaps = bitmaps;
        this.context = context;
        newListener = new DzenSpringListener();
        spring = SpringSystem.create().createSpring().addListener(newListener);
        addView(build(context));
        DzenConfigView.this.setBackgroundColor(0xffffff);
        render();
        this.setVisibility(INVISIBLE);
    }

    private View build(Context context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;

        LayoutParams layoutParams = Util.createMatchParams();
        layoutParams.gravity = Gravity.TOP;

        TypedValue tv = new TypedValue();
        if (getContext().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
            layoutParams.topMargin = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());


        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(Util.createMatchParams());
        frameLayout.setBackgroundColor(Color.argb(100, 0, 0, 0));


       /* ecoGallery = new EcoGallery(context);
        ecoGallery.setUnselectedAlpha(1f);
        ecoGallery.setLayoutParams(layoutParams);
        ecoGallery.setAdapter(new DzenImageAdapter(context, bitmaps));
        ecoGallery.setOnItemClickListener(new EcoGalleryAdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(EcoGalleryAdapterView<?> parent, View view, int position, long id) {
                dzenViewPager.setCurrentItem(position);
                toggle();
                dzenViewPager.toggleLock();
            }
        });*/

       /* ecoGallery.setOnItemSelectedListener(new EcoGalleryAdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(EcoGalleryAdapterView<?> parent, View view, int position, long id) {
                dzenViewPager.setCurrentItem(position);
                toggle();
                dzenViewPager.toggleLock();
            }

            @Override
            public void onNothingSelected(EcoGalleryAdapterView<?> parent) {

            }
        });*/

       // frameLayout.addView(ecoGallery);


        return frameLayout;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (newListener.isVisible) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    y = event.getY();
                    x = event.getX();
                    checkEventMove = false;
                    break;

                case MotionEvent.ACTION_MOVE:
                    float deltaX = event.getX() - x, deltaY = event.getY() - y;
                    if (!checkEventMove) {
                        permisionOpenConfigView = deltaY > 0 && deltaY > Math.abs(deltaX);
                        checkEventMove = true;
                    }

                    if (permisionOpenConfigView) {
                        toggle();
                        dzenViewPager.toggleLock();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    break;

            }
        }
        return super.onInterceptTouchEvent(event);
    }

    /*public void seePageGallery(int position) {
        ecoGallery.setSelection(position);
    }*/

    public DzenConfigView addDzenViewPager(DzenViewPager dzenViewPager) {
        this.dzenViewPager = dzenViewPager;
        return this;
    }


    public void toggle() {
        spring.setEndValue(spring.getEndValue() == 1 ? 0 : 1);
    }

    private void render() {
        float value = (float) spring.getCurrentValue();
        float alpha = value;
        if (value > 1) alpha = 1f;
        else if (value < 0) alpha = 0f;
        DzenConfigView.this.setAlpha(alpha);
        DzenConfigView.this.setTranslationY((float) SpringUtil.mapValueFromRangeToRange(value, 0, 1, height, 0));
    }

    private class DzenSpringListener extends SimpleSpringListener {
        private boolean isVisible = false;


        @Override
        public void onSpringEndStateChange(Spring spring) {
            isVisible = !isVisible;
        }

        @Override
        public void onSpringActivate(Spring spring) {
            if (isVisible) DzenConfigView.this.setVisibility(View.VISIBLE);
            if (!isVisible) {
                DzenConfigView.this.setClickable(false);
                ((Activity) context).getActionBar().hide();
            } else {
                DzenConfigView.this.setClickable(true);
                ((Activity) context).getActionBar().show();
            }
        }

        @Override
        public void onSpringUpdate(Spring spring) {
            render();
        }

        @Override
        public void onSpringAtRest(Spring spring) {
            if (!isVisible) {
                DzenConfigView.this.setVisibility(View.INVISIBLE);

            }
        }

    }


}
