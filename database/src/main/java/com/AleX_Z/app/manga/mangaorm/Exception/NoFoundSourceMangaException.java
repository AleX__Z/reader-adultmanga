package com.AleX_Z.app.manga.mangaorm.Exception;

public class NoFoundSourceMangaException extends SourceMangaException {
    public String getMessage() {
        return "No found source manga";
    }
}
