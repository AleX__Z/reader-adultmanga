package com.AleX_Z.app.manga.login;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import com.dd.CircularProgressButton;
import com.wrapp.floatlabelededittext.FloatLabeledEditText;

/**
 * Created by Alex on 28.09.2014.
 */
public class LogInActivity extends AccountAuthenticatorActivity {
    public static final String EXTRA_TOKEN_TYPE = "com.AleX_Z.app.manga.EXTRA_TOKEN_TYPE";
    private FloatLabeledEditText floatLabeledEditText;
    private CircularProgressButton circularProgressButton;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.login_layout);

        circularProgressButton = (CircularProgressButton)findViewById(R.id.circularProgressButton);
        circularProgressButton.setTextColor(0xff000000);
        circularProgressButton.setIndeterminateProgressMode(true);
        circularProgressButton.setProgress(0);

        circularProgressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (circularProgressButton.getProgress() == 0)
                    submit();
            }
        });

    }

    public void submit() {
        final String username = ((FloatLabeledEditText) findViewById(R.id.username)).getText().toString();
        final String password = ((FloatLabeledEditText) findViewById(R.id.password)).getText().toString();
        final AccountManager am = AccountManager.get(this);
        final Bundle result = new Bundle();

        new AsyncTask<Void,Void,String>() {

            @Override
            protected String doInBackground(Void... params) {
                //TODO make token
                String token = username;

                circularProgressButton.post(new Runnable() {
                    @Override
                    public void run() {
                        circularProgressButton.setTextColor(0xffffffff);
                        circularProgressButton.setProgress(100);
                    }
                });
                try {
                    Thread.sleep(700);
                } catch (InterruptedException e) {

                }
                return token;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                onTokenReceived(new GroupleAccount(username),password,s );
            }
        }.execute();

    }


    public void onTokenReceived(Account account, String password, String token) {
        final AccountManager am = AccountManager.get(this);
        final Bundle result = new Bundle();
        if (am.addAccountExplicitly(account, password, new Bundle())) {
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
            result.putString(AccountManager.KEY_AUTHTOKEN, token);
            am.setAuthToken(account, account.type, token);
        } else {
            result.putString(AccountManager.KEY_ERROR_MESSAGE, "Account already exist");
        }
        setAccountAuthenticatorResult(result);
        setResult(RESULT_OK);

        finish();
    }
}
