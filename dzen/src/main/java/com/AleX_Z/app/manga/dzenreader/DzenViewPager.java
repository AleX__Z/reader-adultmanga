package com.AleX_Z.app.manga.dzenreader;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.handmark.pulltorefresh.extras.viewpager.PullToRefreshViewPager;

/**
 * Created by Alex on 05.07.2014.
 */
public class DzenViewPager extends ViewPager {


    private boolean isLocked, permisionOpenConfigView, checkEventMove;
    private float x, y;
    private DzenConfigView dzenConfigView = null;

    public DzenViewPager(Context context) {
        this(context, null);
    }

    public DzenViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        isLocked = false;
    }

    public DzenViewPager addDzenConfigView(DzenConfigView dzenConfigView) {
        this.dzenConfigView = dzenConfigView;
        return this;

    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (!isLocked) {
            try {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        y = event.getY();
                        x = event.getX();

                        checkEventMove = false;
                        break;

                    case MotionEvent.ACTION_MOVE:
                        float deltaX = event.getX() - x, deltaY = event.getY() - y;
                        if (!checkEventMove) {
                            permisionOpenConfigView = deltaY < 0 && -deltaY > Math.abs(deltaX);
                            checkEventMove = true;
                        }


                        if (permisionOpenConfigView && dzenConfigView != null) {
                            //dzenConfigView.seePageGallery(getCurrentItem());
                            dzenConfigView.toggle();
                            toggleLock();
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        y = 0;
                        break;

                }
                return super.onInterceptTouchEvent(event);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (!isLocked) {


            return super.onTouchEvent(event);
        }

        return false;
    }

    public void toggleLock() {
        isLocked = !isLocked;
    }

    public void setLocked(boolean isLocked) {
        this.isLocked = isLocked;
    }

    public boolean isLocked() {
        return isLocked;
    }


}
