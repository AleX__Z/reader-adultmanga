package com.AleX_Z.app.manga.mangaorm.Lang.DataBase;

import com.AleX_Z.app.manga.mangaorm.Lang.HistoryObject;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by Alex on 23.09.2014.
 */
public class HistoryDao extends BaseDaoImpl<HistoryObject, Integer> {
    protected HistoryDao(ConnectionSource connectionSource,
                       Class<HistoryObject> dataClass) throws SQLException, SQLException {
        super(connectionSource, dataClass);
    }
}
