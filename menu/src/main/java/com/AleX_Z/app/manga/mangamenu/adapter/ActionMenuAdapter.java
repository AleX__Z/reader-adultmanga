package com.AleX_Z.app.manga.mangamenu.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.AleX_Z.app.manga.mangamenu.R;


/**
 * Created by Alex on 22.07.2014.
 */
public class ActionMenuAdapter extends BaseAdapter {
    private static class ViewHolder {
        TextView text;
    }

    private enum VIEW_TYPE {
        TITLE, ITEM
    }


    private Context context;
    private LayoutInflater inflater;
    private String[] titles;
    private String[] urls;
    private int[] icons;

    public ActionMenuAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        final Resources res = context.getResources();
        titles = res.getStringArray(R.array.menu_name);
        urls = res.getStringArray(R.array.menu_url);
        final TypedArray iconsArray = res.obtainTypedArray(R.array.menu_id);
        final int count = iconsArray.length();
        icons = new int[count];
        for (int i = 0; i < count; ++i) {
            icons[i] = iconsArray.getResourceId(i, 0);
        }
        iconsArray.recycle();

    }

    @Override
    public int getCount() {
        return urls.length;
    }

    @Override
    public Uri getItem(int position) {
        return Uri.parse(urls[position]);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final int type = getItemViewType(position);

        final ViewHolder holder;
        if (convertView == null) {
            if (type == VIEW_TYPE.TITLE.ordinal())
                convertView = inflater.inflate(R.layout.category_list_item, parent, false);
            else
                convertView = inflater.inflate(R.layout.action_list_item, parent, false);

            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(android.R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (type != VIEW_TYPE.TITLE.ordinal()) {
            final Drawable icon = convertView.getContext().getResources().getDrawable(icons[position]);
            icon.setBounds(0, 0, icon.getIntrinsicWidth(), icon.getIntrinsicHeight());
            holder.text.setCompoundDrawables(icon, null, null, null);
            holder.text.setText(titles[position]);
        } else {
            holder.text.setText(titles[position].toUpperCase());
        }

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) != VIEW_TYPE.TITLE.ordinal();
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return "title".equals(Uri.parse(urls[position]).getScheme()) ?
                VIEW_TYPE.TITLE.ordinal()
                :
                VIEW_TYPE.ITEM.ordinal();
    }
}
