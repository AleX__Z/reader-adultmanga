package com.AleX_Z.app.manga.mangamenu.fragment;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.AleX_Z.app.manga.mangamenu.R;
import com.AleX_Z.app.manga.mangaorm.Exception.MangaException;
import com.AleX_Z.app.manga.mangaorm.Exception.SourceMangaException;
import com.AleX_Z.app.manga.mangaorm.History.HistoryExplorer;
import com.AleX_Z.app.manga.mangaorm.Lang.DataBase.HelperFactory;
import com.AleX_Z.app.manga.mangaorm.Lang.HistoryObject;
import com.AleX_Z.app.manga.mangaorm.Lang.Manga;
import com.dd.CircularProgressButton;
import com.devspark.progressfragment.ProgressFragment;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

/**
 * Created by Alex on 10.08.2014.
 */
public class InfoMangaFragment extends ProgressFragment {
    public final static String TAG = InfoMangaFragment.class.getSimpleName();
    private Manga manga;
    public boolean searchMode = false;
    public String linkSearchManga = null;
    private ImageView imageView;
    private TextView[] textViews;
    private View view;
    private CircularProgressButton circularProgressButton;
    private int i = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_info_manga, container, false);
        textViews = new TextView[]{(TextView) view.findViewById(R.id.textView)
                , (TextView) view.findViewById(R.id.textView2)};
        imageView = (ImageView) view.findViewById(R.id.imageView);
        circularProgressButton = (CircularProgressButton) view.findViewById(R.id.circularProgressButton);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {

        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText("No Data");
        setContentView(view);
        setContentShown(false);

        if (savedInstanceState != null) {
            searchMode = savedInstanceState.getBoolean("searchMode", searchMode);
            linkSearchManga = savedInstanceState.getString("linkSearchManga");
        }
        if (!searchMode) {
            manga = (Manga) getArguments().getSerializable("manga");
            new InfoAsyncTask().execute(manga);
        } else {
            if (linkSearchManga == null)
                linkSearchManga = getArguments().getString("linkSearchManga");
            circularProgressButton.setVisibility(View.VISIBLE);
            circularProgressButton.setIndeterminateProgressMode(true);
            circularProgressButton.setProgress(i);
            circularProgressButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (circularProgressButton.getProgress() == 0)
                        new AddMangaAsyncTask().execute();
                }
            });


        }
    }


    @Override
    public void onStart() {
        super.onStart();
        new InfoAsyncTask().execute();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("searchMode", searchMode);
        outState.putString("linkSearchManga", linkSearchManga);
        super.onSaveInstanceState(outState);
    }

    class InfoAsyncTask extends AsyncTask<Manga, Void, Bitmap> {
        private ImageLoader imageLoader;
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                .defaultDisplayImageOptions(defaultOptions)
                .build();

        @Override
        protected Bitmap doInBackground(Manga... params) {
            //TODO PICASSO
            imageLoader = ImageLoader.getInstance();
            imageLoader.init(config);
            try {
                if (searchMode)
                    manga = new Manga(new URL(linkSearchManga));
                return imageLoader.loadImageSync(manga.getLinksMainImageManga()[0]);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SourceMangaException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imageView.setImageBitmap(bitmap);
            textViews[1].setText(manga.getDescription());
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Name : ").append(manga.getNames()).append("\n\n");
            stringBuilder.append("Author : ").append(manga.getAuthors()).append("\n\n");
            stringBuilder.append("Year : ").append(manga.getYear()).append("\n\n");
            stringBuilder.append("Genres : ").append(manga.getGenres()).append("\n");
            textViews[0].setText(stringBuilder.toString());
            if (isVisible())
                setContentShown(true);
        }
    }

    class AddMangaAsyncTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            circularProgressButton.setProgress(50);
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                manga.addDataBase().updateChapters();
            } catch (SQLException | MalformedURLException | MangaException e) {
                return -1;
            }
            return 100;
        }

        @Override
        protected void onPostExecute(Integer aVoid) {
            super.onPostExecute(aVoid);
            try {
                //TODO
                HistoryExplorer.addHistory(new HistoryObject("Add manga " + manga.getNames()));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            circularProgressButton.setProgress(aVoid);
            circularProgressButton.setTextColor(0xffffffff);

        }
    }
}
