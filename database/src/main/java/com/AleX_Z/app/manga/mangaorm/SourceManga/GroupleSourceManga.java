package com.AleX_Z.app.manga.mangaorm.SourceManga;

import com.AleX_Z.app.manga.mangaorm.Exception.SourceMangaException;
import com.AleX_Z.app.manga.mangaorm.Lang.Chapter;
import com.AleX_Z.app.manga.mangaorm.Lang.LanguageTranslate;
import com.AleX_Z.app.manga.mangaorm.Lang.Manga;
import com.google.common.io.Closer;

import org.json.JSONArray;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class GroupleSourceManga implements ISourceManga {
    private String[] hosts = new String[]{"adultmanga.ru", "readmanga.me"};

    @Override
    public String[] getHosts() {
        return hosts;
    }


    @Override
    public Manga getManga(URL sourceManga, Manga manga) throws SourceMangaException {
        StringBuilder tmp = new StringBuilder();
        String link = sourceManga.toString();
        Document doc = null;
        try {
            doc = Jsoup.connect(link).get();
        } catch (IOException e) {
            throw new SourceMangaException();
        }

        manga.setSourceManga(link);

        Elements elements = doc.select("body p.elementList>span.elem_genre>a.element-link");
        if (elements.size() != 0) {
            tmp.append(elements.get(0).text());
            for (int i = 1; i < elements.size(); i++)
                tmp.append(",").append(elements.get(i).text());
        }
        manga.setGenres(tmp.toString());

        manga.setDescription(doc.select("body>div.pageBlock meta[itemprop=description]").attr("content"));

        elements = null;
        elements = doc.select("body>.pageBlock div#slider>a[href]");
        if (!elements.isEmpty()) {
            tmp = null;
            tmp = new StringBuilder();
            if (elements.size() != 0) {
                tmp.append(elements.get(0).attr("href"));
                for (int i = 1; i < elements.size(); i++)
                    tmp.append(",").append(elements.get(i).attr("href"));
            }
        } else {
            tmp = null;
            tmp = new StringBuilder();
            elements = doc.select("body>.pageBlock>.manga-page>.subject-cower>img");
            tmp.append(elements.first().attr("src"));
        }
        manga.setLinksMainImageManga(String.valueOf(tmp));


        elements = null;
        elements = doc.select("body>div.pageBlock div.subject-meta>p");

        String volume = elements.get(0).ownText().replaceAll("[[:alpha:],[:space:]]", "");
        try {
            manga.setVolumes(Integer.parseInt(volume));
        } catch (NumberFormatException e) {
            manga.setVolumes(1);
        }


        Elements elements1 = elements.select("span.elem_translator>a.element-link");
        tmp = null;
        tmp = new StringBuilder();
        if (elements1.size() != 0) {
            tmp.append(elements1.get(0).text());
            for (int i = 1; i < elements1.size(); i++)
                tmp.append(",").append(elements1.get(i).text());
        }
        manga.setTranslators(tmp.toString());

        boolean b = "завершен".equals(elements.get(2).ownText());
        manga.setCompleted(b);

        manga.setTranslation(true);

        manga.setAuthors(elements.select("span.elem_author>a.element-link").text());

        String year = elements.select("span.elem_year>a").text();
        try {
            manga.setYear(Integer.parseInt(year));
        } catch (NumberFormatException e) {
            manga.setYear(-1);
        }

        manga.setLanguageTranslate(LanguageTranslate.Russia);

        elements = null;
        elements = doc.select("body>div.pageBlock div.manga-page>h1>span");
        tmp = null;
        tmp = new StringBuilder();
        if (elements.size() != 0) {
            tmp.append(elements.get(0).text());
            for (int i = 1; i < elements.size(); i++)
                tmp.append(",").append(elements.get(i).text());
        }
        manga.setNames(String.valueOf(tmp));

        elements = null;
        elements = doc.select("body>div.pageBlock div.manga-page>div.chapters-link>table td>a");
        manga.setCountChapter(elements.size());

        Date date = new Date();
        manga.setDateCreate(date);
        manga.setDateRead(date);

        return manga;
    }


    public List<Chapter> getChapters(URL url) throws SourceMangaException {
        List<Chapter> chapters = null;
        try {
            chapters = new ArrayList<>();
            String source = url.toString();
            Document doc = Jsoup.connect(source).get();
            Elements elements = doc.select("body>div.pageBlock div.manga-page>div.chapters-link>table tr");
            int size = elements.size();
            source = source.substring(0, source.lastIndexOf("/"));
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
            Chapter chapter;
            Element element, element1;
            String name, link, sDate;
            int volume;

            for (int i = 1; i < size; i++) {
                element = null;
                element = elements.get(i);
                element1 = null;
                element1 = element.select("td>a").first();
                link = source + element1.attr("href") + "?mature=1";
                sDate = element.select("td[align]").first().ownText();
                chapter = new Chapter(
                        element1.ownText().replaceAll("[:cntrl:]", " "),
                        Integer.parseInt(link.substring(link.indexOf("/vol") + 4, link.lastIndexOf("/"))),
                        link,
                        simpleDateFormat.parse(sDate)
                );

                chapters.add(chapter);
            }
            return chapters;
        } catch (IOException | NumberFormatException | ParseException e) {
            throw new SourceMangaException();
        }

    }


    public String getLinksPagesChapter(URL sourceChapter) throws SourceMangaException {
        try {
            StringBuilder stringBuilder = new StringBuilder();

            Closer closer = Closer.create();
            try {
                BufferedReader r = closer.register(new BufferedReader(new InputStreamReader(sourceChapter.openStream())));
                String tmp;
                while ((tmp = r.readLine()) != null) {
                    if (tmp.contains("var pictures")) {
                        String s = tmp.substring(tmp.indexOf("["), tmp.lastIndexOf(";"));
                        JSONArray jsonArray = new JSONArray(s);
                        int size = jsonArray.length();
                        if (size != 0) {
                            stringBuilder.append(jsonArray.getJSONObject(0).getString("url"));
                            for (int i = 1; i < size; i++)
                                stringBuilder.append(",").append(jsonArray.getJSONObject(i).getString("url"));
                        }
                        break;
                    }
                }
            } catch (Throwable e) {
                throw closer.rethrow(e);
            } finally {
                closer.close();
            }
            return stringBuilder.toString();
        } catch (IOException e) {
            throw new SourceMangaException();
        }
    }

    @Override
    public List<PartMangaFeature> search(String searchText, String host) throws SourceMangaException {
        String link = "http://" + host;
        try {
            List<PartMangaFeature> features = new ArrayList<>();
            Document document = Jsoup.connect(link + "/search").data("q", searchText).get();
            Elements elements = document.select("html>body>div#mangaBox>div.leftContent>div#mangaResults>div.tiles>div.tile");
            int size = elements.size();
            if (size == 0) return new ArrayList<PartMangaFeature>();
            PartMangaFeature partMangaFeature;
            Element element, elementImage, elementAuthor, elementName, elementSource;
            String source, author, name, sourceImage;
            for (int i = 0; i < size; i++) {
                element = elements.get(i);
                elementSource = element.select("div.desc>h3>a").first();
                elementAuthor = element.select("div.desc>div.info>a:first-child").first();
                elementName = element.select("div.desc>h3>a").first();
                elementImage = element.select("div.img>a>img").first();

                source = link + elementSource.attr("href");
                name = elementName.attr("title");

                sourceImage = elementImage.attr("src");
                author = (elementAuthor == null) ? "Unknown" : elementAuthor.ownText();


                partMangaFeature = new PartMangaFeature(
                        new URL(source),
                        author,
                        sourceImage,
                        name
                );
                features.add(partMangaFeature);
            }

            return features;
        } catch (IOException e) {
            return new ArrayList<PartMangaFeature>();
        }
    }
}
