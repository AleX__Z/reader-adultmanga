package com.AleX_Z.app.manga.mangaorm.Lang;


import com.AleX_Z.app.manga.mangaorm.Exception.NoFoundSourceMangaException;
import com.AleX_Z.app.manga.mangaorm.Exception.SourceMangaException;
import com.AleX_Z.app.manga.mangaorm.Lang.DataBase.HelperFactory;
import com.AleX_Z.app.manga.mangaorm.SourceManga.SourceManga;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;

@DatabaseTable(tableName = "chapters")
public class Chapter implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "parent_manga")
    private Manga manga;

    @DatabaseField(dataType = DataType.STRING)
    String name;

    @DatabaseField(dataType = DataType.INTEGER)
    int numberChapter;

    @DatabaseField(dataType = DataType.STRING)
    String sourceChapter;

    @DatabaseField(dataType = DataType.INTEGER)
    int volume;

    @DatabaseField(dataType = DataType.INTEGER)
    int countPages;

    @DatabaseField(dataType = DataType.INTEGER)
    int idNextChapter;

    @DatabaseField(dataType = DataType.INTEGER)
    int idPrevChapter;

    //FUCK YOU. I want List<String>, but i don't want to write other special class from Chapter
    @DatabaseField(dataType = DataType.STRING)
    String linksPages;

    @DatabaseField(dataType = DataType.DATE)
    Date dateRead;

    @DatabaseField(dataType = DataType.DATE)
    Date dateCreate;

    @DatabaseField(dataType = DataType.DATE)
    Date dateUpdate;


    Chapter() {
    }

    public Chapter(String name, int volume, String source, Date dateUpdate) {
        dateCreate = new Date();
        this.name = name;
        this.volume = volume;
        this.sourceChapter = source;
        this.dateUpdate = dateUpdate;
        countPages = -1;
    }

    public void update() {
        //TODO
    }


    public int getIdNextChapter() {
        return idNextChapter;
    }

    public void setIdNextChapter(int idNextChapter) {
        this.idNextChapter = idNextChapter;
    }

    public int getIdPrevChapter() {
        return idPrevChapter;
    }

    public void setIdPrevChapter(int idPrevChapter) {
        this.idPrevChapter = idPrevChapter;
    }

    public int getNumberChapter() {
        return numberChapter;
    }

    public void setNumberChapter(int numberChapter) {
        this.numberChapter = numberChapter;
    }

    public String getSourceChapter() {
        return sourceChapter;
    }

    public void setSourceChapter(String sourceChapter) {
        this.sourceChapter = sourceChapter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Manga getManga() {
        return manga;
    }

    public void setManga(Manga manga) {
        this.manga = manga;
    }

    public String[] getLinksPages() throws IOException, SourceMangaException, SQLException {
        if (linksPages == null) {
            linksPages = SourceManga.getLinksPagesChapter(new URL(sourceChapter));
            countPages = linksPages.split(",").length;
            HelperFactory.getInstance().getHelper().getChapterDAO().update(this);
        }
        return linksPages.split(",");
    }

    public void setLinksPages(String linksPages) {
        this.linksPages = linksPages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getCountPages() {
        return countPages;
    }

    public void setCountPages(int countPages) {
        this.countPages = countPages;
    }

    public Date getDateRead() {
        return dateRead;
    }

    public void setDateRead(Date dateRead) {
        this.dateRead = dateRead;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Chapter getNextChapter() {
        if (idNextChapter == 0) return null;
        try {
            return HelperFactory.getInstance().getHelper().getChapterDAO().queryForId(idNextChapter);
        } catch (SQLException e) {
            return null;
        }
    }

    public Chapter getPrevChapter() {
        if (idPrevChapter == 0) return null;
        try {
            return HelperFactory.getInstance().getHelper().getChapterDAO().queryForId(idPrevChapter);
        } catch (SQLException e) {
            return null;
        }
    }

}
