package com.AleX_Z.app.manga.mangaorm.Exception;

public class NoFoundLanguageException extends SourceMangaException {
    public String getMessage() {
        return "Unknown translate language manga from system";
    }
}
