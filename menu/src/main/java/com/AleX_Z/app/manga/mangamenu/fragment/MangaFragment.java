package com.AleX_Z.app.manga.mangamenu.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.AleX_Z.app.manga.mangamenu.MenuActivity;
import com.AleX_Z.app.manga.mangamenu.R;
import com.AleX_Z.app.manga.mangaorm.Lang.Manga;

/**
 * Created by Alex on 10.08.2014.
 */
public class MangaFragment extends Fragment {
    private static final String SCHEME = "item";
    private static final String AUTHORITY = "about_manga";
    public static final Uri URI = new Uri.Builder()
            .scheme(SCHEME)
            .authority(AUTHORITY)
            .build();

    public final static String TAG = MangaFragment.class.getSimpleName();

    private Manga manga;
    private int page = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_manga, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        ((MenuActivity) getActivity()).setUriTag(URI, TAG);
        manga = (Manga) getArguments().getSerializable("manga");
        page = getArguments().getInt("page", 1);
        ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
        viewPager.setAdapter(new MangaPagerAdapter(getFragmentManager()));
        viewPager.setCurrentItem(page);

    }

    private class MangaPagerAdapter extends FragmentStatePagerAdapter {
        private String[] titles = new String[]{"Info", "Chapters"};
        private Fragment[] fragments = new Fragment[titles.length];


        public MangaPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {

            if (fragments[i] == null) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("manga", manga);
                Fragment fragment = (i == 0) ? new InfoMangaFragment() : new ChapterFragment();
                fragment.setArguments(bundle);
                fragments[i] = fragment;
            }
            return fragments[i];
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }


}
