package com.AleX_Z.app.manga.mangaorm.History;

import com.AleX_Z.app.manga.mangaorm.Lang.DataBase.HelperFactory;
import com.AleX_Z.app.manga.mangaorm.Lang.HistoryObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Alex on 23.09.2014.
 */
public class HistoryExplorer {
    public static void addHistory(HistoryObject historyObject) throws SQLException {
        HelperFactory.getInstance().getHelper().getHistoryDAO().create(historyObject);
    }

    public static List<HistoryObject> getListHistory() throws SQLException {
        return HelperFactory.getInstance().getHelper().getHistoryDAO().queryForAll();
    }


    public static void clearAllHistory() throws SQLException {
        HelperFactory.getInstance().getHelper().getHistoryDAO().delete(getListHistory());
    }

}
