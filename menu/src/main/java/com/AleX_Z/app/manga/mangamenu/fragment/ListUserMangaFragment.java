package com.AleX_Z.app.manga.mangamenu.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.AleX_Z.app.manga.mangamenu.R;
import com.AleX_Z.app.manga.mangaorm.History.HistoryExplorer;
import com.AleX_Z.app.manga.mangaorm.Lang.DataBase.HelperFactory;
import com.AleX_Z.app.manga.mangaorm.Lang.DataBase.MangaDao;
import com.AleX_Z.app.manga.mangaorm.Lang.HistoryObject;
import com.AleX_Z.app.manga.mangaorm.Lang.Manga;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardGridArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.internal.CardThumbnail;
import it.gmariotti.cardslib.library.internal.base.BaseCard;
import it.gmariotti.cardslib.library.view.CardGridView;

/**
 * Created by Alex on 22.07.2014.
 */
public class ListUserMangaFragment extends Fragment {
    private ImageLoaderConfiguration config;
    //public static final Uri URI = new Uri.Builder().scheme()
    private static final String SCHEME = "item";
    private static final String AUTHORITY = "manga";
    public static final Uri URI = new Uri.Builder()
            .scheme(SCHEME)
            .authority(AUTHORITY)
            .build();
    public final static String TAG = ListUserMangaFragment.class.getSimpleName();


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_manga, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
            /*.showImageForEmptyUri(com.AleX_Z.app.manga.dzen.R.drawable.akumetsu)*/
                .showImageOnLoading(R.drawable.test)
                .cacheOnDisk(true)
                .build();

        config = new ImageLoaderConfiguration.Builder(getActivity())
                .defaultDisplayImageOptions(defaultOptions)
                .build();

        try {
            init();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void init() throws SQLException {
        MangaDao mangaDao = HelperFactory.getInstance().getHelper().getMangaDAO();
        List<Manga> mangas = mangaDao.queryForAll();
        ArrayList<Card> cards = new ArrayList<Card>();
        for (int i = 0, size = mangas.size(); i < size; i++) {

            cards.add(new MangaCard(getActivity(), mangas.get(i)));
        }

        CardGridArrayAdapter mCardArrayAdapter = new CardGridArrayAdapter(getActivity(), cards);

        CardGridView listView = (CardGridView) getActivity().findViewById(R.id.carddemo_grid_cursor);
        if (listView != null) {
            listView.setAdapter(mCardArrayAdapter);
        }

    }

    public class MangaCard extends Card {
        private Manga manga;

        public MangaCard(Context context, Manga manga) {
            super(context, R.layout.menu_card_inner_layout);
            this.manga = manga;
            initCard();
        }

        private void initCard() {
            CardHeader header = new CardHeader(getContext(), R.layout.menu_card_header_layout);
            header.setButtonOverflowVisible(true);
            header.setTitle(manga.getNames());
            header.setPopupMenu(R.menu.popupmain, new CardHeader.OnClickCardHeaderPopupMenuListener() {
                @Override
                public void onMenuItemClick(BaseCard card, MenuItem item) {
                    int i = item.getItemId();
                    if (i == R.id.info) {
                        final FragmentManager fm = getActivity().getSupportFragmentManager();
                        final FragmentTransaction tr = fm.beginTransaction();
                        Fragment fragment = new MangaFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("manga", ((MangaCard) card).manga);
                        bundle.putInt("page", 0);
                        fragment.setArguments(bundle);
                        tr.addToBackStack(null);
                        tr.replace(R.id.content, fragment);
                        tr.commit();
                    } else if (i == R.id.delete) {
                        try {
                            Manga manga1 = ((MangaCard) card).manga;
                            HistoryExplorer.addHistory(new HistoryObject("Delete Manga " + manga1.getNames()));
                            manga1.removeChapters();
                            MangaDao mangaDao = HelperFactory.getInstance().getHelper().getMangaDAO();
                            mangaDao.delete(manga1);
                            final FragmentManager fm = getActivity().getSupportFragmentManager();
                            final FragmentTransaction tr = fm.beginTransaction();
                            tr.replace(R.id.content, new ListUserMangaFragment());
                            tr.commit();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                    } else if (i == R.id.chapthers) {
                        final FragmentManager fm = getActivity().getSupportFragmentManager();
                        final FragmentTransaction tr = fm.beginTransaction();
                        Fragment fragment = new MangaFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("manga", ((MangaCard) card).manga);
                        fragment.setArguments(bundle);

                        tr.addToBackStack(null);
                        tr.replace(R.id.content, fragment);
                        tr.commit();
                    }

                }
            });
            /**/
            //header.cli
            addCardHeader(header);

            GplayGridThumb thumbnail = new GplayGridThumb(getContext(), manga.getLinksMainImageManga()[0]);


            thumbnail.setUrlResource(manga.getLinksMainImageManga()[0]);

            //thumbnail.setDrawableResource();
            addCardThumbnail(thumbnail);

            setOnClickListener(new OnCardClickListener() {
                @Override
                public void onClick(Card card, View view) {
                    final FragmentManager fm = getActivity().getSupportFragmentManager();
                    final FragmentTransaction tr = fm.beginTransaction();
                    Fragment fragment = new MangaFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("manga", ((MangaCard) card).manga);
                    fragment.setArguments(bundle);
                    tr.addToBackStack(null);
                    tr.replace(R.id.content, fragment);
                    tr.commit();
                }
            });


        }

        @Override
        public void setupInnerViewElements(ViewGroup parent, View view) {
            /*TextView title = (TextView) view.findViewById(R.id.carddemo_cursor_main_inner_title);
            title.setText(String.valueOf(manga.getYear()));*/

            TextView subtitle = (TextView) view.findViewById(R.id.carddemo_cursor_main_inner_subtitle);
            subtitle.setText(manga.getAuthors());



        /*RatingBar mRatingBar = (RatingBar) parent.findViewById(R.id.carddemo_gplay_main_inner_ratingBar);

        mRatingBar.setNumStars(5);
        mRatingBar.setMax(5);
        mRatingBar.setStepSize(0.5f);
        mRatingBar.setRating(5);*/
        }

        class GplayGridThumb extends CardThumbnail {
            private String url;


            public GplayGridThumb(Context context, String url) {
                super(context);
                this.url = url;
            }


            @Override
            public void setupInnerViewElements(ViewGroup parent, View viewImage) {
                /*ImageLoader imageLoader = ImageLoader.getInstance();
                ;

                imageLoader.init(config);


                imageLoader.displayImage(url, (ImageView) viewImage);*/
                Picasso.with(mContext).load(url).into((ImageView)viewImage);
                //setUrlResource(url);
                //viewImage.getLayoutParams().width = 196;
                //viewImage.getLayoutParams().height = 196;
                /*ImageView imageView = (ImageView)parent.findViewById(R.id.card_thumbnail_image);

                imageLoader.displayImage(url,imageView,new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        spinner.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        spinner.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        spinner.setVisibility(View.GONE);
                    }
                });*/


                   /* Picasso.with(getContext())
                            .load(url)

                            .into((ImageView) viewImage);
*/


            }

        }
    }


}
