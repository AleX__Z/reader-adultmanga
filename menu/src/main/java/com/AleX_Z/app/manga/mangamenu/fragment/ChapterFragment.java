package com.AleX_Z.app.manga.mangamenu.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.AleX_Z.app.manga.dzenreader.DzenActivity;

import com.AleX_Z.app.manga.mangamenu.R;
import com.AleX_Z.app.manga.mangaorm.Exception.MangaException;
import com.AleX_Z.app.manga.mangaorm.Exception.SourceMangaException;
import com.AleX_Z.app.manga.mangaorm.Lang.Chapter;
import com.AleX_Z.app.manga.mangaorm.Lang.Manga;
import com.devspark.progressfragment.ProgressListFragment;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Alex on 26.07.2014.
 */
public class ChapterFragment extends ProgressListFragment {
    public Manga manga;

    private List<Chapter> chapters;

    ChapterThread chapterThread;
    SwipeRefreshLayout mSwipeRefreshLayout;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            chapters = (List<Chapter>) msg.obj;
            if (chapters != null) {
                List<String> names = new ArrayList<>();
                for (Chapter chapter : chapters) {
                    names.add(chapter.getName());
                }
                setListAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_list_1, R.id.textView, names));
            }

            final ListView listView = getListView();
            listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    int topRowVerticalPosition =
                            (listView == null || listView.getChildCount() == 0) ?
                                    0 : listView.getChildAt(0).getTop();
                    mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
                }
            });

            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    mSwipeRefreshLayout.setRefreshing(true);
                    setListShown(false);
                    mSwipeRefreshLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            chapterThread = new ChapterThread(handler, true);
                            chapterThread.start();
                        }
                    });

                }
            });
            setListShown(true);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        manga = (Manga) getArguments().getSerializable("manga");
        View view = inflater.inflate(R.layout.fragment_list_chapther, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refreshList);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText("No Data");
        setListShown(false);
        //mSwipeRefreshLayout.setRefreshing(true);
        mSwipeRefreshLayout.setColorScheme(R.color.white, R.color.black, R.color.white, R.color.black);

        if (savedInstanceState != null)
            chapterThread = (ChapterThread) savedInstanceState.getSerializable("thread");

        if (chapterThread == null) {
            chapterThread = new ChapterThread(handler, false);
            chapterThread.start();
        } else
            chapterThread.setHandler(handler);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (chapterThread != null && chapterThread.isAlive()) {
            chapterThread.removeHandler();
            outState.putSerializable("thread", chapterThread);
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(getActivity(), DzenActivity.class);
        Chapter chapter = null;
        Iterator<Chapter> iterator = chapters.iterator();
        for (int i = 0; i <= position; i++) {
            chapter = iterator.next();
        }
        intent.putExtra("chapter", chapter);
        startActivity(intent);
    }


    class ChapterThread extends Thread implements Serializable {
        private boolean update;
        private final Object locker = new Object();
        private Handler tHandler;

        ChapterThread(Handler handler, boolean update) {
            this.tHandler = handler;
            this.update = update;
        }

        public void removeHandler() {
            synchronized (locker) {
                synchronized (tHandler) {
                    this.tHandler = null;
                }
            }
        }

        public void setHandler(Handler handler) {
            synchronized (locker) {
                synchronized (tHandler) {
                    this.tHandler = handler;
                }
                locker.notify();
            }
        }

        @Override
        public void run() {
            mSwipeRefreshLayout.setOnRefreshListener(null);
            List<Chapter> chapters = null;
            try {
                chapters = update ? manga.updateChapters().getChapters(Manga.DATABASE, true) : manga.getChapters(Manga.DATABASE, true);
            } catch (SourceMangaException e) {
                chapters = null;
            } catch (IOException e) {
                chapters = null;
            } catch (MangaException e) {
                chapters = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Message message = new Message();
            message.what = 0;
            message.obj = chapters;
            synchronized (locker) {
                if (handler == null) try {
                    locker.wait();
                } catch (InterruptedException e) {
                    locker.notify();
                }
            }
            handler.sendMessage(message);
        }
    }
}
