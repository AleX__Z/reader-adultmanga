package com.AleX_Z.app.manga.readeradultmanga;

import android.app.Application;

import com.AleX_Z.app.manga.mangaorm.Lang.DataBase.HelperFactory;

/**
 * Created by Alex on 26.07.2014.
 */
public class MangaApplication extends Application {

    public MangaApplication() {
    }

    @Override
    public void onCreate() {
        HelperFactory.getInstance().init(getApplicationContext());
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        HelperFactory.getInstance().release();
        super.onTerminate();
    }
}
