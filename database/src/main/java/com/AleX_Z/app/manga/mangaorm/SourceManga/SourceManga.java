package com.AleX_Z.app.manga.mangaorm.SourceManga;

import com.AleX_Z.app.manga.mangaorm.Exception.NoFoundSourceMangaException;
import com.AleX_Z.app.manga.mangaorm.Exception.SourceMangaException;
import com.AleX_Z.app.manga.mangaorm.Lang.Chapter;
import com.AleX_Z.app.manga.mangaorm.Lang.Manga;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SourceManga {
    private static final ISourceManga[] sourceManga = new ISourceManga[]{new GroupleSourceManga()};


    private static Map<String, ISourceManga> sourceMangaMap;
    public static List<String> hosts;

    static {
        sourceMangaMap = new HashMap<>();
        hosts = new ArrayList<>();
        for (ISourceManga source : sourceManga) {
            for (String host : source.getHosts()) {
                hosts.add(host);
                sourceMangaMap.put(host, source);
            }
        }
    }

    private static ISourceManga getSourceManga(String host) throws NoFoundSourceMangaException {
        ISourceManga result = sourceMangaMap.get(host);
        if (result == null) throw new NoFoundSourceMangaException();
        return result;
    }

    public static Manga getManga(URL urlManga) throws SourceMangaException {
        return getSourceManga(urlManga.getHost()).getManga(urlManga, new Manga());
    }

    public static Manga getManga(URL urlManga, Manga manga) throws SourceMangaException {
        return getSourceManga(urlManga.getHost()).getManga(urlManga, manga);
    }

    public static List<Chapter> getChapters(URL urlManga) throws SourceMangaException {
        return getSourceManga(urlManga.getHost()).getChapters(urlManga);
    }

    public static String getLinksPagesChapter(URL urlChapter) throws SourceMangaException {
        return getSourceManga(urlChapter.getHost()).getLinksPagesChapter(urlChapter);
    }

    public static List<PartMangaFeature> search(String search) throws SourceMangaException {
        return search(search, hosts);
    }

    public static List<PartMangaFeature> search(String search, List<String> filter) throws SourceMangaException {
        List<PartMangaFeature> result = new ArrayList<PartMangaFeature>();
        List<PartMangaFeature> list;
        for (String s : filter) {
            list = getSourceManga(s).search(search, s);
            if (list != null)
                result.addAll(list);
        }
        return result;
    }

    public static void lazyUpdate(Manga manga) {

    }

    public static void chaptersUpdate(Manga manga) {

    }

    public static void update(Manga manga) {

    }


    public static void update(Chapter chapter) {

    }

}





